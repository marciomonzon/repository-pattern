﻿namespace RepositoryPattern.Domain.Entities
{
    public class Carro : EntityBase
    {
        public string Nome { get; private set; }

        public int Potencia { get; private set; }

        public Carro(Guid id, string nome, int potencia) : base(id)
        {
            Nome = nome;
            Potencia = potencia;
        }
    }
}
