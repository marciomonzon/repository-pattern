﻿using RepositoryPattern.Domain.Entities;

namespace RepositoryPattern.Domain.Interfaces
{
    public interface ICarroRepository : IRepositoryBase<Carro>
    {
    }
}
