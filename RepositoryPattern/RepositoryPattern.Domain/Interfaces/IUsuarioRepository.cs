﻿using RepositoryPattern.Domain.Entities;

namespace RepositoryPattern.Domain.Interfaces
{
    public interface IUsuarioRepository
    {
        Task AddAsync(Usuario entity);
    }
}
