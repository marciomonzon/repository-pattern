﻿using RepositoryPattern.Domain.Entities;
using RepositoryPattern.Domain.Interfaces;

namespace RepositoryPattern.Data.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly IRepositoryBase<Usuario> _repositoryBase;

        public UsuarioRepository(IRepositoryBase<Usuario> repositoryBase)
        {
            _repositoryBase = repositoryBase;
        }

        public async Task AddAsync(Usuario entity)
        {
            await _repositoryBase.AddAsync(entity);
        }
    }
}
