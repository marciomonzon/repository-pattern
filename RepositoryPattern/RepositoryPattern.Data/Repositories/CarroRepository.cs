﻿using RepositoryPattern.Data.Context;
using RepositoryPattern.Domain.Entities;
using RepositoryPattern.Domain.Interfaces;

namespace RepositoryPattern.Data.Repositories
{
    public class CarroRepository : RepositoryBase<Carro>, ICarroRepository
    {
        public CarroRepository(AppDbContext appContext) : base(appContext)
        {

        }
    }
}
