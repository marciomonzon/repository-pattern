﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RepositoryPattern.Domain.Entities;
using RepositoryPattern.Domain.Interfaces;

namespace RepositoryPattern.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CarroController : ControllerBase
    {
        private readonly ICarroRepository _carroRepository;

        public CarroController(ICarroRepository carroRepository)
        {
            _carroRepository = carroRepository;
        }

        [HttpPost]
        public async Task<IActionResult> AdicionarCarro()
        {
            var carro = new Carro(Guid.NewGuid(), "Carro", 400);
            await _carroRepository.AddAsync(carro);
            return Ok(carro);
        }
    }
}
