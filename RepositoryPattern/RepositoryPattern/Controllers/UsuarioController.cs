﻿using Microsoft.AspNetCore.Mvc;
using RepositoryPattern.Domain.Entities;
using RepositoryPattern.Domain.Interfaces;

namespace RepositoryPattern.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioController(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        [HttpPost]
        public async Task<IActionResult> AddUsuario()
        {
            var usuario = new Usuario(Guid.NewGuid(), "Cristian", "Email@Email.com", 25);
            await _usuarioRepository.AddAsync(usuario);
            return Ok(usuario);
        }
    }
}
